export default {
  ssr: true,
  target: 'static',
  components: true,
  css: [
    '@/assets/css/main.scss'
  ],
  head: {
    title: 'Honda Civic Sebastix',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'Modified Honda Civic of Sebastix',
        name: 'Honda Civic Sebastix',
        content: 'Modified Honda Civic of Sebastix'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    bodyAttrs: {
      class: ''
    }
  },
  modules: [
    [
      'nuxt-matomo', { matomoUrl: '//matomo.sebastix.nl/', siteId: 7 }
    ]
  ],
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        }
      }
    },
    extend(config) {
      config.resolve.alias.vue = 'vue/dist/vue.common'
    }
  },
  buildModules: [
    '@nuxt/image',
  ],
  image: {
    dir: 'assets/images'
  },
  plugins: [
    { src: '~/plugins/mediagallery', mode: 'client' }
  ]
}