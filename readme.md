# hondacivic.sebastix.com

* NuxtJS

### Used modules and packages
* https://image.nuxtjs.org/
* https://www.npmjs.com/package/vue-matomo
* https://www.npmjs.com/package/vue-cusdis
* https://www.npmjs.com/package/daisyui
* https://www.npmjs.com/package/vue-picture-swipe


### Development

Run development env.
```bash
npm run dev
```
Visit the website locally in your browser at http://localhost:3000

Build and generate.
```bash
npm run build
npm run generate
```

Update.
```bash
npm outdated
npm update
```